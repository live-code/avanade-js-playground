import axios from "axios";

axios.post(
  'https://jsonplaceholder.typicode.com/users',
  {
    name: 'fabio'
  },
  {
    headers: {
      Authorization: 'token123'
    }
  }
)
  .then(res => {
    console.log(res.data)
  })
  .catch(err => {
    console.log('errore!', err)
  })
