import axios from "axios";


// SEQUENTIALS CALLS
const post = await axios.get('https://jsonplaceholder.typicode.com/posts/50');
const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${post.data.userId}` )
console.log(user.data)

// PARALLEL CALLS

const todosAPI = axios.get('https://jsonplaceholder.typicode.com/todosx');
const photosAPI = axios.get('https://jsonplaceholder.typicode.com/photos');

Promise.all([
  todosAPI, photosAPI
])
  .then((res) => {
    console.log(res)
  })
  .catch(err => {
    console.log('ahia 1', err)
  })

Promise.any([
  todosAPI, photosAPI
])
  .then((res) => {
    console.log(res)
  })
  .catch(err => {
    console.log('ahia 2', err)
  })
