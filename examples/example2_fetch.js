console.log('fetch')


//  nested promises
fetch('https://jsonplaceholder.typicode.com/users')
  .then((firstRes) => {
    firstRes.json()
      .then(res => {
        console.log(res)
      })
  })

// chaining promises
fetch('https://jsonplaceholder.typicode.com/users')
  .then((res) => res.json())
  .then(res => {
    // ..
    console.log(res)
  })

// esempio con gestione errori
// genera errore 404 (va nel then, con ok: false
// fetch('https://jsonplaceholder.typicode.com/users/123',)
// genera errore e va nel catch
// fetch('https://XXXXjsonplaceholder.typicode.com/users',)
// esempio con successo
fetch('https://jsonplaceholder.typicode.com/users',)
  .then((res) => {
    if (!res.ok) {
      window.alert('ahia!')
    }
    return res.json()
  })
  .then(res => {
    // ..
    console.log('result', res)
  })
  .catch(err => {
    console.log('ERRORE')
  })



