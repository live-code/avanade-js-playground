import axios from "axios";


// ASYNC AWAIT
// const res = await axios.get('https://jsonplaceholder.typicode.com/posts')
// console.log(res.data)

// example async await with class
class Loader {
   load() {
     return axios.get('https://jsonplaceholder.typicode.com/posts')
  }
}

const p = new Loader()
const res = await p.load()
console.log(res)


// esempio gestione errore
try {
  const users = await axios.get('https://jsonplaceholder.typicode.com/users')
  console.log('here!!!!!!!!!', users.data)
} catch(e) {
  console.log('ahia!')
}

